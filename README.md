# Tactics

**Build your soccer lineup**

![Tactics screenshot](https://gitlab.com/leesonwai/tactics/-/raw/main/data/screenshots/tactics-default.png)

#### Features

* Select from common formation templates or drag-and-drop to create your own
  team shape
* Customize the team's colors to represent your club
* Designate player name and squad numbers
* Export your lineup as an image to save or share

## Install

<a href="https://flathub.org/apps/details/io.gitlab.leesonwai.Tactics"><img src="https://flathub.org/assets/badges/flathub-badge-en.png" width="256px"/></a>

## Build

The recommended way to build Tactics is with GNOME Builder.

#### Dependencies

* `gtk4 >= 4.16.0`
* `libadwaita >= 1.6.0`
