/* tct-exporter.c
 *
 * Copyright 2023 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* This core logic of capturing a GtkWidget in a PNG file is lifted from
 * libadwaita's screenshot.c tool. All credit is due to its author(s).
 */

#include <glib/gi18n.h>

#include "tct-dot.h"
#include "tct-enums.h"
#include "tct-exporter.h"
#include "tct-utils.h"

struct _TctExporter
{
  GObject parent;

  TctExporterScale scale;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (TctExporter, tct_exporter, G_TYPE_OBJECT)
G_DEFINE_QUARK (TctExporterError, tct_exporter_error)

enum
{
  PROP_0,
  PROP_SCALE,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];
static float scales[] = {1.0, 1.5, 2.0};

GFile *
tct_exporter_get_file (void)
{
  g_autoptr (GDateTime) dt = NULL;
  GString *file_str;
  g_autofree const char *filename = NULL;

  dt = g_date_time_new_now_local ();

  file_str = g_string_new (_("Lineup from "));
  g_string_append (file_str, g_date_time_format (dt, "%Y-%m-%d %H∶%M∶%S"));
  g_string_append (file_str, ".png");

  filename = g_string_free_and_steal (file_str);

  return g_file_new_for_path (filename);
}

void
tct_exporter_export (TctExporter  *self,
                     GFile        *file,
                     GtkWidget    *pitch,
                     GError      **error)
{
  int w, h;
  GtkSnapshot *snapshot;
  g_autoptr (GdkPaintable) paintable = NULL;
  float scale;
  g_autoptr (GskRenderNode) node = NULL;
  g_autoptr (GskTransform) transform = NULL;
  g_autoptr (GskRenderNode) transform_node = NULL;
  GskRenderer *renderer;
  g_autoptr (GdkTexture) texture = NULL;
  g_autofree const char *path = NULL;
  g_autofree const char *parse_name = NULL;
  g_autoptr (GBytes) bytes = NULL;

  g_return_if_fail (TCT_IS_EXPORTER (self));
  g_return_if_fail (G_IS_FILE (file));
  g_return_if_fail (GTK_IS_WIDGET (pitch));
  g_return_if_fail (error == NULL || *error == NULL);

  w = gtk_widget_get_width (pitch);
  h = gtk_widget_get_height (pitch);

  snapshot = gtk_snapshot_new ();
  paintable = gtk_widget_paintable_new (pitch);

  gdk_paintable_snapshot (paintable, snapshot, w, h);

  scale = scales[self->scale];
  node = gtk_snapshot_free_to_node (snapshot);
  transform = gsk_transform_new ();
  transform = gsk_transform_scale (transform, scale, scale);
  transform_node = gsk_transform_node_new (node, transform);
  renderer = gtk_native_get_renderer (gtk_widget_get_native (pitch));
  texture = gsk_renderer_render_texture (renderer, transform_node,
                                         &GRAPHENE_RECT_INIT (0, 0,
                                                              w * scale,
                                                              h * scale));

  path = g_file_get_path (file);
  if (path == NULL)
    {
      parse_name = g_file_get_parse_name (file);

      g_set_error (error, TCT_EXPORTER_ERROR,
                   TCT_EXPORTER_ERROR_INVALID_PATH,
                   "Error retrieving path for File: %s", parse_name);

      return;
    }

  bytes = gdk_texture_save_to_png_bytes (texture);
  g_file_replace_contents_bytes_async (file, bytes,
                                       NULL, FALSE,
                                       G_FILE_CREATE_PRIVATE,
                                       NULL, NULL, NULL);
}

TctExporter *
tct_exporter_new (void)
{
  return g_object_new (TCT_TYPE_EXPORTER, NULL);
}

static void
tct_exporter_finalize (GObject *object)
{
  TctExporter *self = TCT_EXPORTER (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (tct_exporter_parent_class)->finalize (object);
}

static void
tct_exporter_set_property (GObject      *object,
                           unsigned int  prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  TctExporter *self = TCT_EXPORTER (object);

  switch (prop_id)
    {
    case PROP_SCALE:
      self->scale = g_value_get_enum (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_exporter_get_property (GObject      *object,
                           unsigned int  prop_id,
                           GValue       *value,
                           GParamSpec   *pspec)
{
  TctExporter *self = TCT_EXPORTER (object);

  switch (prop_id)
    {
    case PROP_SCALE:
      g_value_set_enum (value, self->scale);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_exporter_class_init (TctExporterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = tct_exporter_get_property;
  object_class->set_property = tct_exporter_set_property;
  object_class->finalize = tct_exporter_finalize;

  properties[PROP_SCALE] =
    g_param_spec_enum ("scale", NULL, NULL,
                       TCT_TYPE_EXPORTER_SCALE,
                       TCT_EXPORTER_SCALE_SMALL,
                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
tct_exporter_init (TctExporter *self)
{
  self->settings = g_settings_new ("io.gitlab.leesonwai.Tactics");

  g_settings_bind (self->settings, "exporter-scale",
                   self, "scale",
                   G_SETTINGS_BIND_GET);
}
