/* tct-dot-popover.h
 *
 * Copyright 2022-2023 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TCT_TYPE_DOT_POPOVER (tct_dot_popover_get_type ())

G_DECLARE_FINAL_TYPE (TctDotPopover, tct_dot_popover, TCT, DOT_POPOVER, GtkPopover);

TctDotPopover * tct_dot_popover_new (void);

const char * tct_dot_popover_get_name (TctDotPopover *self);

void tct_dot_popover_set_name (TctDotPopover *self,
                               const char    *name);

uint8_t tct_dot_popover_get_number (TctDotPopover *self);

void tct_dot_popover_set_number (TctDotPopover *self,
                                 uint8_t        number);

G_END_DECLS
