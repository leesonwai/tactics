/* tct-exporter.h
 *
 * Copyright 2023 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "tct-pitch.h"

G_BEGIN_DECLS

#define TCT_EXPORTER_ERROR (tct_exporter_error_quark ())
#define TCT_TYPE_EXPORTER (tct_exporter_get_type ())

typedef enum
{
  TCT_EXPORTER_SCALE_SMALL,
  TCT_EXPORTER_SCALE_MEDIUM,
  TCT_EXPORTER_SCALE_LARGE
} TctExporterScale;

typedef enum
{
  TCT_EXPORTER_ERROR_INVALID_PATH,
} TctExporterError;

G_DECLARE_FINAL_TYPE (TctExporter, tct_exporter, TCT, EXPORTER, GObject)

GQuark tct_exporter_error_quark (void);

void tct_exporter_export (TctExporter  *self,
                          GFile        *file,
                          GtkWidget    *pitch,
                          GError      **error);

GFile * tct_exporter_get_file (void);

TctExporter * tct_exporter_new (void);

G_END_DECLS
