/* tct-pitch.c
 *
 * Copyright 2022-2025 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tct-dot.h"
#include "tct-enums.h"
#include "tct-pitch.h"
#include "tct-utils.h"

#define LINE_WIDTH 6.0
#define N_PLAYERS 11
#define SCALE 6.0

/* Pitch dimensions are in metres. */
#define CENTER_CIRCLE_R ((9.15) * (SCALE))
#define PB_HEIGHT ((16.5) * (SCALE))
#define PB_WIDTH ((40.3) * (SCALE))
#define PENALTY_ARC_R ((9.15) * (SCALE))
#define PITCH_HEIGHT ((105.0) * (SCALE))
#define PITCH_WIDTH ((68.0) * (SCALE))
#define SYB_HEIGHT ((6.0) * (SCALE))
#define SYB_WIDTH ((20.0) * (SCALE))

#define AM_LOW_Y ((PITCH_HEIGHT) * (0.38))
#define AM_HIGH_Y ((PITCH_HEIGHT) * (0.3))
#define CENTER_X ((PITCH_WIDTH) * (0.5))
#define DEFENCE_Y ((PITCH_HEIGHT) * (0.73))
#define DM_Y ((PITCH_HEIGHT) * (0.6))
#define FORWARD_Y ((PITCH_HEIGHT) * (0.21))
#define GK_Y ((PITCH_HEIGHT) * (0.88))
#define LEFT_CENTER_X ((PITCH_WIDTH) * (0.38))
#define LEFT_X ((PITCH_WIDTH) * (0.15))
#define MIDFIELD_Y ((PITCH_HEIGHT) * (0.5))
#define RIGHT_CENTER_X ((PITCH_WIDTH) * (0.62))
#define RIGHT_X ((PITCH_WIDTH) * (0.85))
#define WIDE_LEFT_CENTER_X ((PITCH_WIDTH) * 0.28)
#define WIDE_RIGHT_CENTER_X ((PITCH_WIDTH) * 0.72)

struct _TctPitch
{
  GtkFixed parent;

  GPtrArray *dots;
  TctPitchFormation formation;

  gboolean dots_customized;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (TctPitch, tct_pitch, GTK_TYPE_FIXED)

enum
{
  PROP_0,
  PROP_FORMATION,
  PROP_DOTS_CUSTOMIZED,
  N_PROPS
};

static struct
{
  double x;
  double y;
} const formations[][N_PLAYERS] =
{
  /* 3-4-1-2 */
  {
    /* GK */ {CENTER_X, GK_Y},
    /* RWB */ {RIGHT_X, DM_Y},
    /* LWB */ {LEFT_X, DM_Y},
    /* RCB */ {WIDE_RIGHT_CENTER_X, DEFENCE_Y},
    /* CB */ {CENTER_X, DEFENCE_Y},
    /* LCB */ {WIDE_LEFT_CENTER_X, DEFENCE_Y},
    /* CM */ {RIGHT_CENTER_X, MIDFIELD_Y},
    /* CM */ {LEFT_CENTER_X, MIDFIELD_Y},
    /* ST */ {RIGHT_CENTER_X, FORWARD_Y},
    /* AM */ {CENTER_X, AM_LOW_Y},
    /* ST */ {LEFT_CENTER_X, FORWARD_Y}
  },
  /* 3-4-3 */
  {
    /* GK */ {CENTER_X, GK_Y},
    /* RWB */ {RIGHT_X, DM_Y},
    /* LWB */ {LEFT_X, DM_Y},
    /* RCB */ {WIDE_RIGHT_CENTER_X, DEFENCE_Y},
    /* CB */ {CENTER_X, DEFENCE_Y},
    /* LCB */ {WIDE_LEFT_CENTER_X, DEFENCE_Y},
    /* RW */ {RIGHT_X, AM_HIGH_Y},
    /* CM */ {RIGHT_CENTER_X, MIDFIELD_Y},
    /* ST */ {CENTER_X, FORWARD_Y},
    /* CM */ {LEFT_CENTER_X, MIDFIELD_Y},
    /* LW */ {LEFT_X, AM_HIGH_Y}
  },
  /* 4-2-3-1 */
  {
    /* GK */ {CENTER_X, GK_Y},
    /* RB */ {RIGHT_X, DEFENCE_Y},
    /* LB */ {LEFT_X, DEFENCE_Y},
    /* CB */ {LEFT_CENTER_X, DEFENCE_Y},
    /* CB */ {RIGHT_CENTER_X, DEFENCE_Y},
    /* CM */ {RIGHT_CENTER_X, DM_Y},
    /* RW */ {RIGHT_X, AM_LOW_Y},
    /* CM */ {LEFT_CENTER_X, DM_Y},
    /* ST */ {CENTER_X, FORWARD_Y},
    /* AM */ {CENTER_X, AM_LOW_Y},
    /* LW */ {LEFT_X, AM_LOW_Y},
  },
  /* 4-3-3 */
  {
    /* GK */ {CENTER_X, GK_Y},
    /* RB */ {RIGHT_X, DEFENCE_Y},
    /* LB */ {LEFT_X, DEFENCE_Y},
    /* CB */ {LEFT_CENTER_X, DEFENCE_Y},
    /* CB */ {RIGHT_CENTER_X, DEFENCE_Y},
    /* DM */ {CENTER_X, DM_Y},
    /* RW */ {RIGHT_X, AM_HIGH_Y},
    /* CM */ {RIGHT_CENTER_X, MIDFIELD_Y},
    /* ST */ {CENTER_X, FORWARD_Y},
    /* CM */ {LEFT_CENTER_X, MIDFIELD_Y},
    /* LW */ {LEFT_X, AM_HIGH_Y},
  },
  /* 4-4-2 */
  {
    /* GK */ {CENTER_X, GK_Y},
    /* RB */ {RIGHT_X, DEFENCE_Y},
    /* LB */ {LEFT_X, DEFENCE_Y},
    /* CB */ {LEFT_CENTER_X, DEFENCE_Y},
    /* CB */ {RIGHT_CENTER_X, DEFENCE_Y},
    /* CM */ {RIGHT_CENTER_X, MIDFIELD_Y},
    /* RM */ {RIGHT_X, MIDFIELD_Y},
    /* CM */ {LEFT_CENTER_X, MIDFIELD_Y},
    /* ST */ {RIGHT_CENTER_X, FORWARD_Y},
    /* ST */ {LEFT_CENTER_X, FORWARD_Y},
    /* LM */ {LEFT_X, MIDFIELD_Y},
  }
};
static GParamSpec *properties[N_PROPS];

static void
set_dots_customized (TctPitch *self,
                     gboolean  customized)
{
  g_assert (TCT_IS_PITCH (self));

  customized = !!customized;

  if (self->dots_customized == customized)
    return;

  self->dots_customized = customized;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_DOTS_CUSTOMIZED]);
}

static GskPath *
get_pitch_path (void)
{
  GskPathBuilder *builder;

  builder = gsk_path_builder_new ();

  /* Halfway line. */
  gsk_path_builder_move_to (builder, 0, PITCH_HEIGHT / 2);
  gsk_path_builder_line_to (builder, PITCH_WIDTH, PITCH_HEIGHT / 2);

  /* Center circle. */
  gsk_path_builder_add_circle (builder,
                               &GRAPHENE_POINT_INIT (PITCH_WIDTH / 2,
                                                     PITCH_HEIGHT / 2),
                               CENTER_CIRCLE_R);

  /* Penalty areas. */
  gsk_path_builder_add_rect (builder,
                             &GRAPHENE_RECT_INIT ((PITCH_WIDTH + PB_WIDTH) / 2,
                                                  -LINE_WIDTH,
                                                  -PB_WIDTH,
                                                  PB_HEIGHT));
  gsk_path_builder_add_rect (builder,
                             &GRAPHENE_RECT_INIT ((PITCH_WIDTH + PB_WIDTH) / 2,
                                                  PITCH_HEIGHT + LINE_WIDTH,
                                                  -PB_WIDTH,
                                                  -PB_HEIGHT));

  /* Goal areas. */
  gsk_path_builder_add_rect (builder,
                             &GRAPHENE_RECT_INIT ((PITCH_WIDTH + SYB_WIDTH) / 2,
                                                  -LINE_WIDTH,
                                                  -SYB_WIDTH,
                                                  SYB_HEIGHT));
  gsk_path_builder_add_rect (builder,
                             &GRAPHENE_RECT_INIT ((PITCH_WIDTH + SYB_WIDTH) / 2,
                                                  PITCH_HEIGHT + LINE_WIDTH,
                                                  -SYB_WIDTH,
                                                  -SYB_HEIGHT));

  /* Penalty arcs. */
  gsk_path_builder_move_to (builder,
                            (PITCH_WIDTH / 2) - PENALTY_ARC_R + LINE_WIDTH,
                            PB_HEIGHT - LINE_WIDTH);
  gsk_path_builder_arc_to (builder,
                           (PITCH_WIDTH / 2),
                           PB_HEIGHT + PENALTY_ARC_R,
                           (PITCH_WIDTH / 2) + PENALTY_ARC_R - LINE_WIDTH,
                           PB_HEIGHT - LINE_WIDTH);
  gsk_path_builder_move_to (builder,
                            (PITCH_WIDTH / 2) - PENALTY_ARC_R + LINE_WIDTH,
                            PITCH_HEIGHT - PB_HEIGHT + LINE_WIDTH);
  gsk_path_builder_arc_to (builder,
                           PITCH_WIDTH / 2,
                           PITCH_HEIGHT - PB_HEIGHT - PENALTY_ARC_R,
                           (PITCH_WIDTH / 2) + PENALTY_ARC_R,
                           PITCH_HEIGHT - PB_HEIGHT + LINE_WIDTH);

  return gsk_path_builder_free_to_path (builder);
}

static void
save_dot_props (TctPitch *self)
{
  g_autoptr (GVariantBuilder) builder = NULL;
  TctDot *dot;
  GVariant *variant;

  g_assert (TCT_IS_PITCH (self));

  builder = g_variant_builder_new (G_VARIANT_TYPE ("a(sydd)"));
  for (int i = 0; i < N_PLAYERS; i++)
    {
      dot = g_ptr_array_index (self->dots, i);

      g_variant_builder_add (builder, "(sydd)",
                             tct_dot_get_name (dot),
                             tct_dot_get_number (dot),
                             tct_dot_get_x (dot),
                             tct_dot_get_y (dot));
    }
  variant = g_variant_builder_end (builder);

  g_settings_set_value (self->settings, "dot-props", variant);
}

static void
load_dot_props (TctPitch *self)
{
  g_autoptr (GVariantIter) iter = NULL;
  GVariant *variant;
  TctDot *dot;
  const char *name;
  uint8_t number;
  double x, y;

  g_assert (TCT_IS_PITCH (self));

  g_settings_get (self->settings, "dot-props", "a(sydd)", &iter);

  if (g_variant_iter_n_children (iter) == 0)
    {
      tct_pitch_arrange_formation (self);
      return;
    }

  for (int i = 0; i < N_PLAYERS && (variant = g_variant_iter_next_value (iter)); i++)
    {
      dot = g_ptr_array_index (self->dots, i);

      g_variant_get (variant, "(sydd)", &name, &number, &x, &y);

      tct_dot_set_name (dot, name);
      tct_dot_set_number (dot, number);
      tct_dot_set_x (dot, x);
      tct_dot_set_y (dot, y);
    }
}

static void
on_formation_changed (TctPitch *self)
{
  g_assert (TCT_IS_PITCH (self));

  tct_pitch_arrange_formation (self);
}

void
tct_pitch_set_formation (TctPitch          *self,
                         TctPitchFormation  formation)
{
  g_return_if_fail (TCT_IS_PITCH (self));

  if (self->formation == formation)
    return;

  self->formation = formation;

  tct_pitch_arrange_formation (self);
}

TctPitchFormation
tct_pitch_get_formation (TctPitch *self)
{
  g_return_val_if_fail (TCT_IS_PITCH (self), 0);

  return self->formation;
}

void
tct_pitch_reset_dots (TctPitch *self)
{
  TctDot *dot;

  g_return_if_fail (TCT_IS_PITCH (self));

  for (int i = 0; i < N_PLAYERS; i++)
    {
      dot = g_ptr_array_index (self->dots, i);

      tct_dot_set_name (dot, "");
      tct_dot_set_number (dot, 9);
    }

  set_dots_customized (self, FALSE);
}

void
tct_pitch_arrange_formation (TctPitch *self)
{
  int offset_x, offset_y;
  TctDot *dot;

  g_return_if_fail (TCT_IS_PITCH (self));

  for (int i = 0; i < N_PLAYERS; i++)
    {
      dot = g_ptr_array_index (self->dots, i);

      tct_dot_get_offsets (dot, &offset_x, &offset_y);
      tct_dot_set_x (dot, formations[self->formation][i].x - offset_x);
      tct_dot_set_y (dot, formations[self->formation][i].y - offset_y);
    }
}

static void
on_dot_notify_name_number (TctPitch *self)
{
  g_assert (TCT_IS_PITCH (self));

  set_dots_customized (self, TRUE);
}

static void
on_dot_notify_x_y (GObject    *object,
                   GParamSpec *pspec,
                   gpointer    user_data)
{
  TctDot *dot = TCT_DOT (object);
  TctPitch *self = TCT_PITCH (user_data);

  g_assert (TCT_IS_PITCH (self));

  gtk_fixed_move (GTK_FIXED (self),
                  GTK_WIDGET (dot),
                  tct_dot_get_x (dot),
                  tct_dot_get_y (dot));
}

TctPitch *
tct_pitch_new (void)
{
  return g_object_new (TCT_TYPE_PITCH, NULL);
}

static void
tct_pitch_snapshot (GtkWidget   *widget,
                    GtkSnapshot *snapshot)
{
  g_autoptr (GskPath) path = NULL;
  TctPitch *self = TCT_PITCH (widget);
  GskStroke *stroke;
  GdkRGBA line_color;

  path = get_pitch_path ();
  stroke = gsk_stroke_new (LINE_WIDTH);

  gtk_widget_get_color (GTK_WIDGET (self), &line_color);
  gtk_snapshot_append_stroke (snapshot, path, stroke, &line_color);

  gsk_stroke_free (stroke);

  GTK_WIDGET_CLASS (tct_pitch_parent_class)->snapshot (widget, snapshot);
}

static void
tct_pitch_finalize (GObject *object)
{
  TctPitch *self = TCT_PITCH (object);

  g_assert (TCT_IS_PITCH (self));

  g_clear_pointer (&self->dots, g_ptr_array_unref);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (tct_pitch_parent_class)->finalize (object);
}

static void
tct_pitch_dispose (GObject *object)
{
  TctPitch *self = TCT_PITCH (object);

  save_dot_props (self);
  gtk_widget_dispose_template (GTK_WIDGET (self), TCT_TYPE_PITCH);

  G_OBJECT_CLASS (tct_pitch_parent_class)->dispose (object);
}

static void
tct_pitch_set_property (GObject      *object,
                        unsigned int  prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  TctPitch *self = TCT_PITCH (object);

  switch (prop_id)
    {
    case PROP_FORMATION:
      tct_pitch_set_formation (self, g_value_get_enum (value));
      break;

    case PROP_DOTS_CUSTOMIZED:
      self->dots_customized = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_pitch_get_property (GObject      *object,
                        unsigned int  prop_id,
                        GValue       *value,
                        GParamSpec   *pspec)
{
  TctPitch *self = TCT_PITCH (object);

  switch (prop_id)
    {
    case PROP_FORMATION:
      g_value_set_enum (value, tct_pitch_get_formation (self));
      break;

    case PROP_DOTS_CUSTOMIZED:
      g_value_set_boolean (value, self->dots_customized);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_pitch_class_init (TctPitchClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->get_property = tct_pitch_get_property;
  object_class->set_property = tct_pitch_set_property;
  object_class->dispose = tct_pitch_dispose;
  object_class->finalize = tct_pitch_finalize;

  widget_class->snapshot = tct_pitch_snapshot;

  properties[PROP_FORMATION] =
    g_param_spec_enum ("formation", NULL, NULL,
                       TCT_TYPE_PITCH_FORMATION,
                       TCT_PITCH_FORMATION_442,
                       G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_DOTS_CUSTOMIZED] =
    g_param_spec_boolean ("dots-customized", NULL, NULL,
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Tactics/tct-pitch.ui");
}

static void
tct_pitch_init (TctPitch *self)
{
  TctDot *dot;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->dots = g_ptr_array_new ();
  self->settings = g_settings_new ("io.gitlab.leesonwai.Tactics");

  gtk_widget_set_size_request (GTK_WIDGET (self), PITCH_WIDTH, PITCH_HEIGHT);

  for (int i = 0; i < N_PLAYERS; i++)
    {
      dot = tct_dot_new ();

      gtk_widget_set_parent (GTK_WIDGET (dot), GTK_WIDGET (self));
      g_ptr_array_add (self->dots, dot);

      g_signal_connect (dot, "notify::x", G_CALLBACK (on_dot_notify_x_y), self);
      g_signal_connect (dot, "notify::y", G_CALLBACK (on_dot_notify_x_y), self);
      g_signal_connect_swapped (dot, "notify::name", G_CALLBACK (on_dot_notify_name_number), self);
      g_signal_connect_swapped (dot, "notify::number", G_CALLBACK (on_dot_notify_name_number), self);
    }

  g_settings_bind (self->settings, "formation",
                   self, "formation",
                   G_SETTINGS_BIND_GET);

  g_signal_connect_swapped (self->settings, "changed::formation", G_CALLBACK (on_formation_changed), self);

  load_dot_props (self);
}
