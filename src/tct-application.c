/* tct-application.c
 *
 * Copyright 2022-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "tct-application.h"
#include "tct-window.h"

#define ADD_ACCEL(app,action,...) { \
                                    const char *tmp[] = {__VA_ARGS__, NULL}; \
                                    gtk_application_set_accels_for_action (GTK_APPLICATION (app), \
                                                                           action, \
                                                                           tmp); \
                                  }

struct _TctApplication
{
  AdwApplication parent;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (TctApplication, tct_application, ADW_TYPE_APPLICATION)

static gboolean show_version;

static void
quit_activated (GSimpleAction *simple,
                GVariant      *parameter,
                gpointer       user_data)
{
  TctApplication *self = TCT_APPLICATION (user_data);

  g_assert (TCT_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}

static void
about_activated (GSimpleAction *simple,
                 GVariant      *parameter,
                 gpointer       user_data)
{
  AdwDialog *dialog;
  GtkApplication *self = GTK_APPLICATION (user_data);
  const char *artists[] =
  {
    "Google Inc.",
    "OpenMoji",
    NULL
  };
  const char *developers[] =
  {
    "Joshua Lee <lee.son.wai@gmail.com>",
    NULL
  };

  g_assert (GTK_IS_APPLICATION (self));

  dialog = adw_about_dialog_new_from_appdata ("/io/gitlab/leesonwai/Tactics/metainfo", NULL);
  adw_about_dialog_set_application_icon (ADW_ABOUT_DIALOG (dialog), ICON_NAME);
  adw_about_dialog_set_artists (ADW_ABOUT_DIALOG (dialog), artists);
  adw_about_dialog_set_copyright (ADW_ABOUT_DIALOG (dialog), "© 2022-2024 Joshua Lee");
  adw_about_dialog_set_developers (ADW_ABOUT_DIALOG (dialog), developers);

  adw_dialog_present (dialog, GTK_WIDGET (gtk_application_get_active_window (self)));
}

static void
exporter_scale_changed (GSimpleAction *action,
                        GVariant      *parameter,
                        gpointer       user_data)
{
  TctApplication *self = TCT_APPLICATION (user_data);
  const char *scale;

  g_assert (TCT_IS_APPLICATION (self));

  scale = g_variant_get_string (parameter, NULL);

  g_simple_action_set_state (action, parameter);
  g_settings_set_string (self->settings, "exporter-scale", scale);
}

TctApplication *
tct_application_new (void)
{
  return g_object_new (TCT_TYPE_APPLICATION,
                       "application-id", APP_ID,
                       "resource-base-path", "/io/gitlab/leesonwai/Tactics",
                       NULL);
}

static void
tct_application_activate (GApplication *app)
{
  GtkWindow *window;
  TctApplication *self = TCT_APPLICATION (app);

  g_assert (TCT_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  if (!window)
    window = GTK_WINDOW (tct_window_new (self));

  gtk_window_present (window);
}

static void
tct_application_startup (GApplication *app)
{
  const GActionEntry app_entries[] =
  {
    {. name = "exporter_scale", .parameter_type = "s", .state = "'small'", .change_state = exporter_scale_changed},
    {.name = "about", .activate = about_activated},
    {.name = "quit", .activate = quit_activated}
  };
  GAction *action;
  GVariant *variant;
  TctApplication *self = TCT_APPLICATION (app);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries,
                                   G_N_ELEMENTS (app_entries),
                                   app);

  ADD_ACCEL (app, "app.quit", "<Ctrl>Q");

  action = g_action_map_lookup_action (G_ACTION_MAP (app), "exporter_scale");
  variant = g_variant_new_string (g_settings_get_string (self->settings, "exporter-scale"));
  g_simple_action_set_state (G_SIMPLE_ACTION (action), variant);

  G_APPLICATION_CLASS (tct_application_parent_class)->startup (app);
}

static void
tct_application_finalize (GObject *object)
{
  TctApplication *self = TCT_APPLICATION (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (tct_application_parent_class)->finalize (object);
}

static int
tct_application_handle_local_options (GApplication *app,
                                      GVariantDict *options)
{
  if (show_version)
    {
      g_print ("Tactics %s\n", VERSION);

      return 0;
    }

  return -1;
}

static void
tct_application_class_init (TctApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = tct_application_finalize;

  app_class->handle_local_options = tct_application_handle_local_options;
  app_class->startup = tct_application_startup;
  app_class->activate = tct_application_activate;
}

static void
tct_application_init (TctApplication *self)
{
  const GOptionEntry main_entries[] =
  {
    {"version", 'V', 0, G_OPTION_ARG_NONE, &show_version, "Print version information and exit", NULL},
    {NULL}
  };

  self->settings = g_settings_new ("io.gitlab.leesonwai.Tactics");

  g_application_add_main_option_entries (G_APPLICATION (self), main_entries);
}
