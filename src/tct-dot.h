/* tct-dot.h
 *
 * Copyright 2022-2023 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TCT_TYPE_DOT (tct_dot_get_type ())

G_DECLARE_FINAL_TYPE (TctDot, tct_dot, TCT, DOT, GtkBox)

const char * tct_dot_get_name (TctDot *self);

void tct_dot_set_name (TctDot     *self,
                       const char *name);

TctDot * tct_dot_new (void);

uint8_t tct_dot_get_number (TctDot *self);

void tct_dot_set_number (TctDot        *self,
                         const uint8_t  number);

const GdkRGBA * tct_dot_get_number_color (TctDot *self);

void tct_dot_set_number_color (TctDot        *self,
                               const GdkRGBA *color);

void tct_dot_get_offsets (TctDot *self,
                          int    *offset_x,
                          int    *offset_y);

const GdkRGBA * tct_dot_get_primary_color (TctDot *self);

void tct_dot_set_primary_color (TctDot        *self,
                                const GdkRGBA *color);

const GdkRGBA * tct_dot_get_secondary_color (TctDot *self);

void tct_dot_set_secondary_color (TctDot        *self,
                                  const GdkRGBA *color);

double tct_dot_get_x (TctDot *self);

void tct_dot_set_x (TctDot *self,
                    double  x);

double tct_dot_get_y (TctDot *self);

void tct_dot_set_y (TctDot *self,
                    double  y);

G_END_DECLS
