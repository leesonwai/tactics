/* tct-utils.c
 *
 * Copyright 2022 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tct-utils.h"

gboolean
map_string_to_rgba (GValue   *value,
                    GVariant *variant,
                    gpointer  user_data)
{
  const char *rgba_str;
  GdkRGBA rgba;

  rgba_str = g_variant_get_string (variant, NULL);

  gdk_rgba_parse (&rgba, rgba_str);
  g_value_set_boxed (value, gdk_rgba_copy (&rgba));

  return TRUE;
}

GVariant *
map_rgba_to_string (const GValue       *value,
                    const GVariantType *expected_type,
                    gpointer            user_data)
{
  GdkRGBA *rgba;
  const char *rgba_str;
  GVariant *variant;

  rgba = g_value_get_boxed (value);
  rgba_str = gdk_rgba_to_string (rgba);
  variant = g_variant_new_string (rgba_str);

  return variant;
}
