/* tct-dot.c
 *
 * Copyright 2022-2025 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tct-dot.h"
#include "tct-dot-popover.h"
#include "tct-utils.h"

#define DOT_NAME_MARGIN 6
#define DOT_R 24
#define LINE_WIDTH 6
#define MAX_WIDTH_CHARS 12

struct _TctDot
{
  GtkBox parent;

  GtkButton *dot_button;
  GtkLabel *number_label;
  GtkLabel *name_label;
  TctDotPopover *popover;

  char *name;
  uint8_t number;
  GdkRGBA *primary_color;
  GdkRGBA *secondary_color;
  GdkRGBA *number_color;

  double x;
  double y;
  int offset;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (TctDot, tct_dot, GTK_TYPE_BOX)

enum
{
  PROP_0,
  PROP_X,
  PROP_Y,
  PROP_NAME,
  PROP_NUMBER,
  PROP_PRIMARY_COLOR,
  PROP_SECONDARY_COLOR,
  PROP_NUMBER_COLOR,
  N_PROPS
};

static void clamp_to_name_width (TctDot *self);

static void on_dot_popover_closed (TctDot *self);

static void on_dot_popover_notify_name (TctDot *self);

static void on_dot_popover_notify_number (TctDot *self);

static GParamSpec *properties[N_PROPS];

static void
update_name_label_and_center (TctDot *self)
{
  int new_offset;

  g_assert (TCT_IS_DOT (self));

  gtk_label_set_text (self->name_label, tct_dot_get_name (self));
  clamp_to_name_width (self);
  tct_dot_get_offsets (self, &new_offset, NULL);
  tct_dot_set_x (self, tct_dot_get_x (self) - new_offset + self->offset);

  self->offset = new_offset;
}

static void
dot_popover_popup (TctDot *self)
{
  g_assert (TCT_IS_DOT (self));

  self->popover = tct_dot_popover_new ();

  gtk_widget_set_parent (GTK_WIDGET (self->popover),
                         GTK_WIDGET (self->dot_button));

  tct_dot_popover_set_name (self->popover, tct_dot_get_name (self));
  tct_dot_popover_set_number (self->popover, tct_dot_get_number (self));

  g_signal_connect_swapped (self->popover, "closed", G_CALLBACK (on_dot_popover_closed), self);
  g_signal_connect_swapped (self->popover, "notify::name", G_CALLBACK (on_dot_popover_notify_name), self);
  g_signal_connect_swapped (self->popover, "notify::number", G_CALLBACK (on_dot_popover_notify_number), self);

  gtk_popover_popup (GTK_POPOVER (self->popover));
}

static void
clamp_to_name_width (TctDot *self)
{
  const char *name;
  int name_len;

  g_assert (TCT_IS_DOT (self));

  name = tct_dot_get_name (self);
  /* This can happen before TctDot is constructed. */
  if (name == NULL)
    return;

  name_len = g_utf8_strlen (name, -1) > MAX_WIDTH_CHARS
                                        ? MAX_WIDTH_CHARS
                                        : g_utf8_strlen (name, -1);

  gtk_label_set_width_chars (self->name_label, name_len);
  gtk_label_set_max_width_chars (self->name_label, name_len);
}

static GskPath *
build_dot_path (TctDot *self)
{
  GskPathBuilder *builder;
  gboolean computed;
  graphene_point_t center;

  g_assert (TCT_IS_DOT (self));

  builder = gsk_path_builder_new ();
  computed = gtk_widget_compute_point (GTK_WIDGET (self->dot_button),
                                       GTK_WIDGET (self),
                                       &GRAPHENE_POINT_INIT (DOT_R, DOT_R),
                                       &center);

  g_assert (computed);

  gsk_path_builder_add_circle (builder, &center, DOT_R - DOT_NAME_MARGIN);

  return gsk_path_builder_free_to_path (builder);
}

void
tct_dot_set_y (TctDot *self,
               double  y)
{
  g_return_if_fail (TCT_IS_DOT (self));

  if (self->y == y)
    return;

  self->y = y;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_Y]);
}

double
tct_dot_get_y (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), 0);

  return self->y;
}

void
tct_dot_set_x (TctDot *self,
               double  x)
{
  g_return_if_fail (TCT_IS_DOT (self));

  if (self->x == x)
    return;

  self->x = x;

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_X]);
}

double
tct_dot_get_x (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), 0);

  return self->x;
}

void
tct_dot_set_secondary_color (TctDot        *self,
                             const GdkRGBA *color)
{
  g_return_if_fail (TCT_IS_DOT (self));
  g_return_if_fail (color != NULL);

  if (gdk_rgba_equal (self->secondary_color, color))
    return;

  gdk_rgba_free (self->secondary_color);

  self->secondary_color = gdk_rgba_copy (color);

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

const GdkRGBA *
tct_dot_get_secondary_color (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), NULL);

  return self->secondary_color;
}

void
tct_dot_set_primary_color (TctDot        *self,
                           const GdkRGBA *color)
{
  g_return_if_fail (TCT_IS_DOT (self));
  g_return_if_fail (color != NULL);

  if (gdk_rgba_equal (self->primary_color, color))
    return;

  gdk_rgba_free (self->primary_color);

  self->primary_color = gdk_rgba_copy (color);

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

const GdkRGBA *
tct_dot_get_primary_color (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), NULL);

  return self->primary_color;
}

void
tct_dot_get_offsets (TctDot *self,
                     int    *offset_x,
                     int    *offset_y)
{
  g_return_if_fail (TCT_IS_DOT (self));

  gtk_widget_measure (GTK_WIDGET (self),
                      GTK_ORIENTATION_HORIZONTAL,
                      -1, offset_x, NULL, NULL, NULL);

  gtk_widget_measure (GTK_WIDGET (self),
                      GTK_ORIENTATION_VERTICAL,
                      -1, offset_y, NULL, NULL, NULL);

  /* Ensure that the height of name_label is always discounted so that
   * offset_y is consistent whether its visible or not.
   */
  if (offset_y != NULL && gtk_widget_is_visible (GTK_WIDGET (self)))
    *offset_y -= gtk_widget_get_height (GTK_WIDGET (self->name_label));

  if (offset_x != NULL)
    *offset_x /= 2;
  if (offset_y != NULL)
    *offset_y /= 2;
}

void
tct_dot_set_number_color (TctDot        *self,
                          const GdkRGBA *color)
{
  g_autoptr (PangoAttrList) attr_list = NULL;
  uint16_t r, g, b;
  PangoAttribute *color_attr, *alpha_attr;

  g_return_if_fail (TCT_IS_DOT (self));
  g_return_if_fail (color != NULL);

  if (gdk_rgba_equal (self->number_color, color))
    return;

  gdk_rgba_free (self->number_color);

  self->number_color = gdk_rgba_copy (color);

  attr_list = pango_attr_list_new ();
  r = (uint16_t) (self->number_color->red * UINT16_MAX);
  g = (uint16_t) (self->number_color->green * UINT16_MAX);
  b = (uint16_t) (self->number_color->blue * UINT16_MAX);
  color_attr = pango_attr_foreground_new (r, g, b);
  alpha_attr = pango_attr_foreground_alpha_new (UINT16_MAX);

  pango_attr_list_insert (attr_list, color_attr);
  pango_attr_list_insert (attr_list, alpha_attr);

  gtk_label_set_attributes (self->number_label, attr_list);
}

const GdkRGBA *
tct_dot_get_number_color (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), NULL);

  return self->number_color;
}

void
tct_dot_set_number (TctDot        *self,
                    const uint8_t  number)
{
  g_autofree const char *number_str = NULL;

  g_return_if_fail (TCT_IS_DOT (self));
  g_return_if_fail (number < 100);

  if (self->number == number)
    return;

  self->number = number;

  number_str = g_strdup_printf ("%u", number);

  gtk_label_set_text (self->number_label, number_str);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NUMBER]);
}

uint8_t
tct_dot_get_number (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), 0);

  return self->number;
}

void
tct_dot_set_name (TctDot     *self,
                  const char *name)
{
  g_return_if_fail (TCT_IS_DOT (self));
  g_return_if_fail (name != NULL);

  if (g_strcmp0 (self->name, name) == 0)
    return;

  g_free (self->name);

  self->name = g_strdup (name);

  update_name_label_and_center (self);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
}

const char *
tct_dot_get_name (TctDot *self)
{
  g_return_val_if_fail (TCT_IS_DOT (self), NULL);

  return self->name;
}

static void
on_dot_popover_notify_name (TctDot *self)
{
  g_assert (TCT_IS_DOT (self));

  tct_dot_set_name (self, tct_dot_popover_get_name (self->popover));
}

static void
on_dot_popover_notify_number (TctDot *self)
{
  g_assert (TCT_IS_DOT (self));

  tct_dot_set_number (self, tct_dot_popover_get_number (self->popover));
}

static void
on_dot_popover_closed (TctDot *self)
{
  g_assert (TCT_IS_DOT (self));

  gtk_widget_unparent (GTK_WIDGET (self->popover));

  g_clear_object (&self->popover);
}

static void
on_name_label_notify_visible (TctDot *self)
{
  g_assert (TCT_IS_DOT (self));

  update_name_label_and_center (self);
}

static void
on_drag_update (TctDot   *self,
                double    offset_x,
                double    offset_y,
                gpointer  user_data)
{
  int dot_w, dot_h, pitch_w, pitch_h;
  double x, y;
  GtkWidget *pitch;

  g_assert (TCT_IS_DOT (self));

  pitch = gtk_widget_get_parent (GTK_WIDGET (self));

  dot_w = gtk_widget_get_width (GTK_WIDGET (self));
  dot_h = gtk_widget_get_height (GTK_WIDGET (self));
  pitch_w = gtk_widget_get_width (pitch);
  pitch_h = gtk_widget_get_height (pitch);
  x = tct_dot_get_x (self) + offset_x;
  y = tct_dot_get_y (self) + offset_y;

  if (x + dot_w >= pitch_w)
    x = pitch_w - dot_w;
  if (x <= 0)
    x = 0;
  if (y + dot_h >= pitch_h)
    y = pitch_h - dot_h;
  if (y <= 0)
    y = 0;

  tct_dot_set_x (self, x);
  tct_dot_set_y (self, y);
}

static void
on_drag_end (GtkGestureDrag *drag,
             double          offset_x,
             double          offset_y,
             gpointer        user_data)
{
  gtk_event_controller_reset (GTK_EVENT_CONTROLLER (drag));
}

static void
on_secondary_button_pressed (TctDot *self)
{
  g_assert (TCT_IS_DOT (self));

  dot_popover_popup (self);
}

static void
on_key_released (GtkEventControllerKey *key,
                 unsigned int           keyval,
                 unsigned int           keycode,
                 GdkModifierType        state,
                 gpointer               user_data)
{
  TctDot *self = TCT_DOT (user_data);

  g_assert (TCT_IS_DOT (self));

  if (keyval != GDK_KEY_Return)
    return;

  dot_popover_popup (self);
}

TctDot *
tct_dot_new (void)
{
  return g_object_new (TCT_TYPE_DOT, NULL);
}

static void
tct_dot_snapshot (GtkWidget   *widget,
                  GtkSnapshot *snapshot)
{
  g_autoptr (GskPath) path = NULL;
  TctDot *self = TCT_DOT (widget);
  GskStroke *stroke;

  path = build_dot_path (self);
  stroke = gsk_stroke_new (LINE_WIDTH);

  gtk_snapshot_append_fill (snapshot, path,
                            GSK_FILL_RULE_WINDING, self->primary_color);
  gtk_snapshot_append_stroke (snapshot, path, stroke, self->secondary_color);

  gsk_stroke_free (stroke);

  GTK_WIDGET_CLASS (tct_dot_parent_class)->snapshot (widget, snapshot);
}

static void
tct_dot_finalize (GObject *object)
{
  TctDot *self = TCT_DOT (object);

  g_clear_object (&self->popover);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->primary_color, gdk_rgba_free);
  g_clear_pointer (&self->secondary_color, gdk_rgba_free);
  g_clear_pointer (&self->number_color, gdk_rgba_free);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (tct_dot_parent_class)->finalize (object);
}

static void
tct_dot_dispose (GObject *object)
{
  TctDot *self = TCT_DOT (object);

  gtk_widget_dispose_template (GTK_WIDGET (self), TCT_TYPE_DOT);

  G_OBJECT_CLASS (tct_dot_parent_class)->dispose (object);
}

static void
tct_dot_constructed (GObject *object)
{
  TctDot *self = TCT_DOT (object);

  gtk_widget_set_size_request (GTK_WIDGET (self->dot_button), DOT_R * 2, DOT_R * 2);
  tct_dot_get_offsets (self, &self->offset, NULL);
  clamp_to_name_width (self);

  G_OBJECT_CLASS (tct_dot_parent_class)->constructed (object);
}

static void
tct_dot_set_property (GObject      *object,
                      unsigned int  prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  TctDot *self = TCT_DOT (object);

  switch (prop_id)
    {
    case PROP_X:
      tct_dot_set_x (self, g_value_get_double (value));
      break;

    case PROP_Y:
      tct_dot_set_y (self, g_value_get_double (value));
      break;

    case PROP_NUMBER:
      tct_dot_set_number (self, g_value_get_uchar (value));
      break;

    case PROP_NAME:
      tct_dot_set_name (self, g_value_get_string (value));
      break;

    case PROP_PRIMARY_COLOR:
      tct_dot_set_primary_color (self, g_value_get_boxed (value));
      break;

    case PROP_SECONDARY_COLOR:
      tct_dot_set_secondary_color (self, g_value_get_boxed (value));
      break;

    case PROP_NUMBER_COLOR:
      tct_dot_set_number_color (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_dot_get_property (GObject      *object,
                      unsigned int  prop_id,
                      GValue       *value,
                      GParamSpec   *pspec)
{
  TctDot *self = TCT_DOT (object);

  switch (prop_id)
    {
    case PROP_X:
      g_value_set_double (value, tct_dot_get_x (self));
      break;

    case PROP_Y:
      g_value_set_double (value, tct_dot_get_y (self));
      break;

    case PROP_NUMBER:
      g_value_set_uchar (value, tct_dot_get_number (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, tct_dot_get_name (self));
      break;

    case PROP_PRIMARY_COLOR:
      g_value_set_boxed (value, tct_dot_get_primary_color (self));
      break;

    case PROP_SECONDARY_COLOR:
      g_value_set_boxed (value, tct_dot_get_secondary_color (self));
      break;

    case PROP_NUMBER_COLOR:
      g_value_set_boxed (value, tct_dot_get_number_color (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_dot_class_init (TctDotClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->get_property = tct_dot_get_property;
  object_class->set_property = tct_dot_set_property;
  object_class->constructed = tct_dot_constructed;
  object_class->dispose = tct_dot_dispose;
  object_class->finalize = tct_dot_finalize;

  widget_class->snapshot = tct_dot_snapshot;

  properties[PROP_X] =
    g_param_spec_double ("x", NULL, NULL,
                         0.0, DBL_MAX, 0.0,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties[PROP_Y] =
    g_param_spec_double ("y", NULL, NULL,
                         0.0, DBL_MAX, 0.0,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties[PROP_NUMBER] =
    g_param_spec_uchar ("number", NULL, NULL,
                        1, 99, 9,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS);

  properties[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         "",
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS);

  properties[PROP_PRIMARY_COLOR] =
    g_param_spec_boxed ("primary-color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SECONDARY_COLOR] =
    g_param_spec_boxed ("secondary-color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_NUMBER_COLOR] =
    g_param_spec_boxed ("number-color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Tactics/tct-dot.ui");

  gtk_widget_class_bind_template_child (widget_class, TctDot, dot_button);
  gtk_widget_class_bind_template_child (widget_class, TctDot, number_label);
  gtk_widget_class_bind_template_child (widget_class, TctDot, name_label);

  gtk_widget_class_bind_template_callback (widget_class, on_key_released);
  gtk_widget_class_bind_template_callback (widget_class, on_secondary_button_pressed);
  gtk_widget_class_bind_template_callback (widget_class, on_drag_end);
  gtk_widget_class_bind_template_callback (widget_class, on_drag_update);
  gtk_widget_class_bind_template_callback (widget_class, on_name_label_notify_visible);
}

static void
tct_dot_init (TctDot *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->primary_color = g_new (GdkRGBA, 1);
  self->secondary_color = g_new (GdkRGBA, 1);
  self->number_color = g_new (GdkRGBA, 1);
  self->settings = g_settings_new ("io.gitlab.leesonwai.Tactics");

  g_settings_bind_with_mapping (self->settings, "primary-color",
                                self, "primary-color",
                                G_SETTINGS_BIND_GET,
                                map_string_to_rgba,
                                map_rgba_to_string,
                                NULL, NULL);

  g_settings_bind_with_mapping (self->settings, "secondary-color",
                                self, "secondary-color",
                                G_SETTINGS_BIND_GET,
                                map_string_to_rgba,
                                map_rgba_to_string,
                                NULL, NULL);

  g_settings_bind_with_mapping (self->settings, "number-color",
                                self, "number-color",
                                G_SETTINGS_BIND_GET,
                                map_string_to_rgba,
                                map_rgba_to_string,
                                NULL, NULL);

  g_settings_bind (self->settings, "names-visible",
                   self->name_label, "visible",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "numbers-visible",
                   self->number_label, "visible",
                   G_SETTINGS_BIND_DEFAULT);
}
