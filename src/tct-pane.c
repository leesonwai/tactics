/*
 * tct-pane.c
 *
 * Copyright 2023-2025 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tct-enums.h"
#include "tct-pane.h"
#include "tct-pitch.h"
#include "tct-utils.h"

struct _TctPane
{
  AdwPreferencesPage parent;

  AdwSwitchRow *names_switch;
  AdwSwitchRow *numbers_switch;
  AdwComboRow *formation_combo;
  GtkColorDialogButton *primary_color_button;
  GtkColorDialogButton *secondary_color_button;
  GtkColorDialogButton *number_color_button;
  AdwButtonRow *clear_row;

  GSettings *settings;
};

enum
{
  PROP_0,
  PROP_CLEAR_ROW_SENSITIVE,
  N_PROPS
};

enum
{
  SIGNAL_CLEAR_ROW_ACTIVATED,
  N_SIGNALS
};

G_DEFINE_FINAL_TYPE (TctPane, tct_pane, ADW_TYPE_PREFERENCES_PAGE)

static GParamSpec *properties[N_PROPS];
static unsigned int signals[N_SIGNALS];

static gboolean
map_formation_to_uint (GValue   *value,
                       GVariant *variant,
                       gpointer  user_data)
{
  GEnumClass *enum_class;
  GEnumValue *enum_value;

  enum_class = g_type_class_ref (TCT_TYPE_PITCH_FORMATION);
  enum_value = g_enum_get_value_by_nick (enum_class,
                                         g_variant_get_string (variant, NULL));

  g_value_set_uint (value, enum_value->value);

  return TRUE;
}

static GVariant *
map_uint_to_formation (const GValue       *value,
                       const GVariantType *expected_type,
                       gpointer            user_data)
{
  GEnumClass *enum_class;
  GEnumValue *enum_value;

  enum_class = g_type_class_ref (TCT_TYPE_PITCH_FORMATION);
  enum_value = g_enum_get_value (enum_class, g_value_get_uint (value));

  return g_variant_new_string (enum_value->value_nick);
}

static void
on_clear_row_activated (TctPane *self)
{
  g_signal_emit (self, signals[SIGNAL_CLEAR_ROW_ACTIVATED], 0, NULL);
}

static char *
format_formation_name_cb (AdwEnumListItem *item,
                          gpointer         user_data)
{
  switch (adw_enum_list_item_get_value (item))
    {
    case TCT_PITCH_FORMATION_3412:
      return g_strdup ("3–4–1–2");

    case TCT_PITCH_FORMATION_343:
      return g_strdup ("3–4–3");

    case TCT_PITCH_FORMATION_4231:
      return g_strdup ("4–2–3–1");

    case TCT_PITCH_FORMATION_433:
      return g_strdup ("4–3–3");

    case TCT_PITCH_FORMATION_442:
      return g_strdup ("4–4–2");

    default:
      g_assert_not_reached ();
    }
}

static void
tct_pane_finalize (GObject *object)
{
  TctPane *self = TCT_PANE (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (tct_pane_parent_class)->finalize (object);
}

static void
tct_pane_dispose (GObject *object)
{
  TctPane *self = TCT_PANE (object);

  gtk_widget_dispose_template (GTK_WIDGET (self), TCT_TYPE_PANE);

  G_OBJECT_CLASS (tct_pane_parent_class)->dispose (object);
}

static void
tct_pane_set_property (GObject      *object,
                       unsigned int  prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
  TctPane *self = TCT_PANE (object);

  switch (prop_id)
    {
    case PROP_CLEAR_ROW_SENSITIVE:
      gtk_widget_set_sensitive (GTK_WIDGET (self->clear_row), g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_pane_get_property (GObject      *object,
                       unsigned int  prop_id,
                       GValue       *value,
                       GParamSpec   *pspec)
{
  TctPane *self = TCT_PANE (object);

  switch (prop_id)
    {
    case PROP_CLEAR_ROW_SENSITIVE:
      g_value_set_boolean (value, gtk_widget_get_sensitive (GTK_WIDGET (self->clear_row)));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_pane_class_init (TctPaneClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->get_property = tct_pane_get_property;
  object_class->set_property = tct_pane_set_property;
  object_class->dispose = tct_pane_dispose;
  object_class->finalize = tct_pane_finalize;

  properties[PROP_CLEAR_ROW_SENSITIVE] =
    g_param_spec_boolean ("clear-row-sensitive", NULL, NULL,
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  signals[SIGNAL_CLEAR_ROW_ACTIVATED] =
    g_signal_new ("clear-row-activated",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL,
                  G_TYPE_NONE, 0);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Tactics/tct-pane.ui");
  gtk_widget_class_bind_template_child (widget_class, TctPane, names_switch);
  gtk_widget_class_bind_template_child (widget_class, TctPane, numbers_switch);
  gtk_widget_class_bind_template_child (widget_class, TctPane, formation_combo);
  gtk_widget_class_bind_template_child (widget_class, TctPane, primary_color_button);
  gtk_widget_class_bind_template_child (widget_class, TctPane, secondary_color_button);
  gtk_widget_class_bind_template_child (widget_class, TctPane, number_color_button);
  gtk_widget_class_bind_template_child (widget_class, TctPane, clear_row);

  gtk_widget_class_bind_template_callback (widget_class, format_formation_name_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_clear_row_activated);

  g_type_ensure (TCT_TYPE_PITCH_FORMATION);
}

static void
tct_pane_init (TctPane *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("io.gitlab.leesonwai.Tactics");

  g_settings_bind (self->settings, "names-visible",
                   self->names_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "numbers-visible",
                   self->numbers_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind_with_mapping (self->settings, "formation",
                                self->formation_combo, "selected",
                                G_SETTINGS_BIND_DEFAULT,
                                map_formation_to_uint,
                                map_uint_to_formation,
                                NULL, NULL);

  g_settings_bind_with_mapping (self->settings, "primary-color",
                                self->primary_color_button, "rgba",
                                G_SETTINGS_BIND_DEFAULT,
                                map_string_to_rgba,
                                map_rgba_to_string,
                                NULL, NULL);

  g_settings_bind_with_mapping (self->settings, "secondary-color",
                                self->secondary_color_button, "rgba",
                                G_SETTINGS_BIND_DEFAULT,
                                map_string_to_rgba,
                                map_rgba_to_string,
                                NULL, NULL);

  g_settings_bind_with_mapping (self->settings, "number-color",
                                self->number_color_button, "rgba",
                                G_SETTINGS_BIND_DEFAULT,
                                map_string_to_rgba,
                                map_rgba_to_string,
                                NULL, NULL);
}
