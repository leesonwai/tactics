/* tct-dot-popover.c
 *
 * Copyright 2022-2023 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tct-dot.h"
#include "tct-dot-popover.h"

struct _TctDotPopover
{
  GtkPopover parent;

  GtkListBox *list_box;
  AdwEntryRow *name_entry;
  AdwSpinRow *number_spin_row;
};

G_DEFINE_FINAL_TYPE (TctDotPopover, tct_dot_popover, GTK_TYPE_POPOVER)

enum
{
  PROP_0,
  PROP_NAME,
  PROP_NUMBER,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

void
tct_dot_popover_set_number (TctDotPopover *self,
                            uint8_t        number)
{
  g_return_if_fail (TCT_IS_DOT_POPOVER (self));
  g_return_if_fail (number < 100);

  if ((uint8_t) adw_spin_row_get_value (self->number_spin_row) == number)
    return;

  adw_spin_row_set_value (self->number_spin_row, number);
}

uint8_t
tct_dot_popover_get_number (TctDotPopover *self)
{
  g_return_val_if_fail (TCT_IS_DOT_POPOVER (self), 0);

  return (uint8_t) adw_spin_row_get_value (self->number_spin_row);
}

void
tct_dot_popover_set_name (TctDotPopover *self,
                          const char    *name)
{
  g_return_if_fail (TCT_IS_DOT_POPOVER (self));
  g_return_if_fail (name != NULL);

  if (g_strcmp0 (gtk_editable_get_text (GTK_EDITABLE (self->name_entry)), name) == 0)
    return;

  gtk_editable_set_text (GTK_EDITABLE (self->name_entry), name);
}

const char *
tct_dot_popover_get_name (TctDotPopover *self)
{
  g_return_val_if_fail (TCT_IS_DOT_POPOVER (self), NULL);

  return gtk_editable_get_text (GTK_EDITABLE (self->name_entry));
}

static void
on_number_spin_row_notify_value (TctDotPopover *self)
{
  g_assert (TCT_IS_DOT_POPOVER (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NUMBER]);
}

static void
on_name_entry_changed (TctDotPopover *self)
{
  g_assert (TCT_IS_DOT_POPOVER (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
}

static void
on_realize (GtkWidget *widget,
            gpointer   user_data)
{
  TctDotPopover *self = TCT_DOT_POPOVER (widget);

  /* This is to prevent name_entry grabbing focus when
   * the popover is shown.
   */
  gtk_widget_grab_focus (GTK_WIDGET (self->list_box));
}

TctDotPopover *
tct_dot_popover_new (void)
{
  return g_object_new (TCT_TYPE_DOT_POPOVER, NULL);
}

static void
tct_dot_popover_dispose (GObject *object)
{
  TctDotPopover *self = TCT_DOT_POPOVER (object);

  gtk_widget_dispose_template (GTK_WIDGET (self), TCT_TYPE_DOT_POPOVER);

  G_OBJECT_CLASS (tct_dot_popover_parent_class)->dispose (object);
}

static void
tct_dot_popover_set_property (GObject      *object,
                              unsigned int  prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  TctDotPopover *self = TCT_DOT_POPOVER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      tct_dot_popover_set_name (self, g_value_get_string (value));
      break;

    case PROP_NUMBER:
      tct_dot_popover_set_number (self, g_value_get_uchar (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_dot_popover_get_property (GObject      *object,
                              unsigned int  prop_id,
                              GValue       *value,
                              GParamSpec   *pspec)
{
  TctDotPopover *self = TCT_DOT_POPOVER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, tct_dot_popover_get_name (self));
      break;

    case PROP_NUMBER:
      g_value_set_uchar (value, tct_dot_popover_get_number (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
tct_dot_popover_class_init (TctDotPopoverClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->get_property = tct_dot_popover_get_property;
  object_class->set_property = tct_dot_popover_set_property;
  object_class->dispose = tct_dot_popover_dispose;

  properties[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties[PROP_NUMBER] =
    g_param_spec_uchar ("number", NULL, NULL,
                        1, 99, 9,
                        G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Tactics/tct-dot-popover.ui");
  gtk_widget_class_bind_template_child (widget_class, TctDotPopover, list_box);
  gtk_widget_class_bind_template_child (widget_class, TctDotPopover, name_entry);
  gtk_widget_class_bind_template_child (widget_class, TctDotPopover, number_spin_row);

  gtk_widget_class_bind_template_callback (widget_class, on_realize);
  gtk_widget_class_bind_template_callback (widget_class, on_name_entry_changed);
  gtk_widget_class_bind_template_callback (widget_class, on_number_spin_row_notify_value);
}

static void
tct_dot_popover_init (TctDotPopover *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
