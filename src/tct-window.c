/* tct-window.c
 *
 * Copyright 2022-2025 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "tct-exporter.h"
#include "tct-pane.h"
#include "tct-pitch.h"
#include "tct-window.h"

struct _TctWindow
{
  AdwApplicationWindow parent;

  GtkToggleButton *pane_button;
  AdwOverlaySplitView *split_view;
  AdwToastOverlay *toast_overlay;
  TctPitch *pitch;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (TctWindow, tct_window, ADW_TYPE_APPLICATION_WINDOW)

static void
on_response (GObject      *source_object,
             GAsyncResult *result,
             gpointer      user_data)
{
  g_autoptr (GFile) file = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (TctExporter) exporter = NULL;
  TctWindow *self = TCT_WINDOW (user_data);
  g_autofree const char *uri = NULL;

  g_assert (TCT_IS_WINDOW (self));

  file = gtk_file_dialog_save_finish (GTK_FILE_DIALOG (source_object),
                                      result,
                                      &error);
  if (file == NULL)
    {
      g_warning ("Failed to save file: %s", error->message);

      return;
    }

  exporter = tct_exporter_new ();
  tct_exporter_export (exporter, file, GTK_WIDGET (self->pitch), &error);

  if (error)
    {
      adw_toast_overlay_add_toast (self->toast_overlay,
                                   adw_toast_new ("Unable to save lineup"));
      g_warning ("Failed to save file: %s", error->message);

      return;
    }

  uri = g_file_get_uri (file);
  gtk_recent_manager_add_item (gtk_recent_manager_get_default (), uri);
}

static void
toggle_pane (GSimpleAction *simple,
             GVariant      *parameter,
             gpointer       user_data)
{
  TctWindow *self = TCT_WINDOW (user_data);
  gboolean show_sidebar;

  show_sidebar = adw_overlay_split_view_get_show_sidebar (self->split_view);

  adw_overlay_split_view_set_show_sidebar (self->split_view, !show_sidebar);
}

static void
reset_activated (GSimpleAction *simple,
                 GVariant      *parameter,
                 gpointer       user_data)
{
  TctWindow *self = TCT_WINDOW (user_data);

  tct_pitch_arrange_formation (self->pitch);
}

static void
export_activated (GSimpleAction *simple,
                  GVariant      *parameter,
                  gpointer       user_data)
{
  g_autoptr (GtkFileDialog) dialog = NULL;
  g_autoptr (GFile) file = NULL;
  const char *pictures_dir;
  g_autoptr (GFile) initial_dir = NULL;
  TctWindow *self = TCT_WINDOW (user_data);

  g_assert (TCT_IS_WINDOW (self));

  dialog = gtk_file_dialog_new ();
  file = tct_exporter_get_file ();

  pictures_dir = g_get_user_special_dir (G_USER_DIRECTORY_PICTURES);
  if (pictures_dir != NULL)
    initial_dir = g_file_new_for_path (pictures_dir);
  else
    initial_dir = g_file_new_for_path (g_get_home_dir ());

  gtk_file_dialog_set_initial_file (dialog, file);
  gtk_file_dialog_set_initial_folder (dialog, initial_dir);
  gtk_file_dialog_save (dialog, GTK_WINDOW (self),
                        NULL,
                        (GAsyncReadyCallback) on_response,
                        self);
}

static void
on_clear_row_activated (TctWindow *self)
{
  tct_pitch_reset_dots (self->pitch);
}

static void
on_refresh_button_clicked (TctPitch *pitch)
{
  g_assert (TCT_IS_PITCH (pitch));

  tct_pitch_arrange_formation (pitch);
}

TctWindow *
tct_window_new (TctApplication *app)
{
  return g_object_new (TCT_TYPE_WINDOW,
                       "application", app,
                       NULL);
}

static void
tct_window_finalize (GObject *object)
{
  TctWindow *self = TCT_WINDOW (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (tct_window_parent_class)->finalize (object);
}

static void
tct_window_dispose (GObject *object)
{
  TctWindow *self = TCT_WINDOW (object);

  gtk_widget_dispose_template (GTK_WIDGET (self), TCT_TYPE_WINDOW);

  G_OBJECT_CLASS (tct_window_parent_class)->dispose (object);
}

static void
tct_window_constructed (GObject *object)
{
  TctWindow *self = TCT_WINDOW (object);
  const GActionEntry win_entries[] =
  {
    {.name = "export", .activate = export_activated},
    {.name = "reset-formation", .activate = reset_activated},
    {.name = "toggle-pane", .activate = toggle_pane}
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   win_entries,
                                   G_N_ELEMENTS (win_entries),
                                   self);

#if DEVEL_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

  if (g_settings_get_boolean (self->settings, "window-maximized"))
    gtk_window_maximize (GTK_WINDOW (self));

  G_OBJECT_CLASS (tct_window_parent_class)->constructed (object);
}

static void
tct_window_class_init (TctWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = tct_window_constructed;
  object_class->dispose = tct_window_dispose;
  object_class->finalize = tct_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Tactics/tct-window.ui");

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_SHIFT_MASK | GDK_CONTROL_MASK, "win.export", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_r, GDK_CONTROL_MASK, "win.reset-formation", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_F9, 0, "win.toggle-pane", NULL);

  gtk_widget_class_bind_template_child (widget_class, TctWindow, pane_button);
  gtk_widget_class_bind_template_child (widget_class, TctWindow, split_view);
  gtk_widget_class_bind_template_child (widget_class, TctWindow, toast_overlay);
  gtk_widget_class_bind_template_child (widget_class, TctWindow, pitch);

  gtk_widget_class_bind_template_callback (widget_class, on_refresh_button_clicked);
  gtk_widget_class_bind_template_callback (widget_class, on_clear_row_activated);

  g_type_ensure (TCT_TYPE_PANE);
  g_type_ensure (TCT_TYPE_PITCH);
}

static void
tct_window_init (TctWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("io.gitlab.leesonwai.Tactics");

  g_settings_bind (self->settings, "window-height",
                   self, "default-height",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "window-maximized",
                   self, "maximized",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "window-width",
                   self, "default-width",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "show-pane",
                   self->pane_button, "active",
                   G_SETTINGS_BIND_DEFAULT);
}
