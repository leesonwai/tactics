/* tct-pitch.h
 *
 * Copyright 2022-2025 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TCT_TYPE_PITCH (tct_pitch_get_type ())

typedef enum
{
  TCT_PITCH_FORMATION_3412,
  TCT_PITCH_FORMATION_343,
  TCT_PITCH_FORMATION_4231,
  TCT_PITCH_FORMATION_433,
  TCT_PITCH_FORMATION_442
} TctPitchFormation;

G_DECLARE_FINAL_TYPE (TctPitch, tct_pitch, TCT, PITCH, GtkFixed)

void tct_pitch_arrange_formation (TctPitch *self);

void tct_pitch_reset_dots (TctPitch *self);

TctPitchFormation tct_pitch_get_formation (TctPitch *self);

void tct_pitch_set_formation (TctPitch          *self,
                              TctPitchFormation  formation);

TctPitch * tct_pitch_new (void);

G_END_DECLS
